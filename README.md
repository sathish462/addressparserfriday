

## Task Description

An address provider returns addresses only with concatenated street names and numbers. Our own system on the other hand has separate fields for street name and street number.

Input: string of address
Output: string of street and string of street-number as JSON object

eg: "Winterallee 3" -> {"street": "Winterallee", "housenumber": "3"}

Note : only one address will be sent at a time. Since each example is single JSON object.

### Technologies

* Java 11
* Spring Boot
* Docker
* Junit
* Mockito
* maven

### Design Decisions

Task 1 : Implemented a controller for Address Parser<br/>

### Some examples of implemented functionality

1.Parse valid Address : HTTP Method
    
    http://localhost:8080/v1/addressParser
    
    Input test data :
    
    {
        "address": "Winterallee 3"
    }
    
    HTTP response code 200 : OK
    
    {
        "street": "Winterallee",
        "houseNumber": "3"
    }

2. Parse invalid address : HTTP Method
               
        http://localhost:8080/v1/addressParser
        
        Input test data :
            
        {
            "address": "JunkAddress@Junk"
        }
           
        HTTP response code 400 : BAD REQUEST
           
        {
            "message": "No Parser found.",
            "details": [
                          "Parser not found for given address :  JunkAddress@Junk"
            ]
        }
           

### Swagger documentation

Swagger documentation is accessible at the following URL once the containers are started:

http://localhost:8080/swagger-ui.html#

### Commands

#### To Run With Docker

Start docker Address Parser API containers (must be in project working directory): docker-compose -f .\docker-compose.yml up

#### To run with mvn

(must be in project working directory): mvn spring-boot:run

#### To run tests

(must be in project working directory): mvn test