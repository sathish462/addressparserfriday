package com.friday.addressParser.testData;

import com.friday.addressParser.dto.AddressDto;
import com.friday.addressParser.dto.ResponseAddressDto;

public enum ESampleTestData {
    SIMPLE_ADDRESS_1          (new AddressDto("Winterallee 3"),new ResponseAddressDto("Winterallee","3")),
    SIMPLE_ADDRESS_2          (new AddressDto("Musterstrasse 45"),new ResponseAddressDto("Musterstrasse","45")),
    SIMPLE_ADDRESS_3          (new AddressDto("Blaufeldweg 123B"),new ResponseAddressDto("Blaufeldweg","123B")),

    COMPLICATED_ADDRESS_1     (new AddressDto("Am Bächle 23"),new ResponseAddressDto("Am Bächle","23")),
    COMPLICATED_ADDRESS_2     (new AddressDto("Auf der Vogelwiese 23 b"),new ResponseAddressDto("Auf der Vogelwiese","23 b")),

    INTERNATIONAL_ADDRESS_1   (new AddressDto("4, rue de la revolution"),new ResponseAddressDto("rue de la revolution","4")),
    INTERNATIONAL_ADDRESS_2   (new AddressDto("200 Broadway Av"),new ResponseAddressDto("Broadway Av","200")),
    INTERNATIONAL_ADDRESS_3   (new AddressDto("Calle Aduana, 29"),new ResponseAddressDto("Calle Aduana","29")),
    INTERNATIONAL_ADDRESS_4   (new AddressDto("Calle 39 No 1540"),new ResponseAddressDto("Calle 39","No 1540"));

    ESampleTestData(AddressDto inputAddress, ResponseAddressDto outPutAddress) {
        key = inputAddress;
        value = outPutAddress;
    }

    //public static final Map<AddressDto,ResponseAddressDTO> map;
    private AddressDto key;
    private ResponseAddressDto value;

  /*  static
    {
        map = new HashMap<AddressDto,ResponseAddressDTO>();
        for(ESampleTestData data : ESampleTestData.values())
        {
            map.put(data.getKey(),data.getValue());
        }
    }*/

    public AddressDto getKey() {
        return key;
    }

    public void setKey(AddressDto key) {
        this.key = key;
    }

    public ResponseAddressDto getValue() {
        return value;
    }

    public void setValue(ResponseAddressDto value) {
        this.value = value;
    }

}
