package com.friday.addressParser.controller;

import com.friday.addressParser.Util.UtilTest;
import com.friday.addressParser.dto.AddressDto;
import com.friday.addressParser.exception.EmptyAddressException;
import com.friday.addressParser.service.ParseAddressService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.mockito.Mockito;

@WithMockUser
@WebMvcTest(AddressParserController.class)
@ExtendWith(SpringExtension.class)
public class AddressParserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ParseAddressService parseAddressService;

    /**
     * Test - call the service method to check if the service is being called once.
     */
    @Test
    public void parseAddressServiceTest() throws Exception
    {
        parseAddressService.getParsedAddress(UtilTest.getTestAddressDTO());
        Mockito.verify(parseAddressService,Mockito.times(1)).getParsedAddress(UtilTest.getTestAddressDTO());
    }

    @Test
    public void testParseAddress() throws Exception {
        AddressDto incomingAddressDTO = UtilTest.getTestAddressDTO();

        MvcResult result = mockMvc.perform(post("/v1/addressParser").accept(MediaType.APPLICATION_JSON)
                                                                             .content(UtilTest.objectToJson(incomingAddressDTO))
                                                                             .contentType(MediaType.APPLICATION_JSON))
                                                                             .andReturn();

        int status = result.getResponse().getStatus();
        assertEquals(HttpStatus.OK.value(), status);
    }

    @Test
    public void testParseAddressWithInvalidData() throws Exception {
        AddressDto incomingAddressDTO = UtilTest.getInvalidAddressDTO();

        Mockito.when(parseAddressService.getParsedAddress(Mockito.any(AddressDto.class))).thenThrow(new EmptyAddressException("Empty address cannot be parsed."));

        MvcResult result = mockMvc.perform(post("/v1/addressParser").accept(MediaType.APPLICATION_JSON)
                .content(UtilTest.objectToJson(incomingAddressDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        int status = result.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), status);
    }

}
