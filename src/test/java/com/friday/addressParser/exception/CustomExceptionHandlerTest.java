package com.friday.addressParser.exception;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class CustomExceptionHandlerTest {

    @Autowired
    CustomExceptionHandler customExceptionHandler = new CustomExceptionHandler();

    @Test
    public void handleAllExceptions() {
        ResponseEntity<Object> responseEntity =
                customExceptionHandler.handleAllExceptions(new Exception("Exception"),
                        new MockHttpServletRequest());

        assertEquals(((ErrorResponse)responseEntity.getBody()).getMessage(),"Server Error");
    }

    @Test
    public void handleInvalidInputException()
    {
        ResponseEntity<Object> responseEntity =
                customExceptionHandler.handleInvalidInputException(new EmptyAddressException("Exception"),
                        new MockHttpServletRequest());

        assertEquals(((ErrorResponse)responseEntity.getBody()).getMessage(),"Empty address cannot be parsed.");
    }

    @Test
    public void handleParserNotFoundException()
    {
        ResponseEntity<Object> responseEntity =
                customExceptionHandler.handleParserNotFoundException(new ParserNotFoundException("Exception"),
                        new MockHttpServletRequest());

        assertEquals(((ErrorResponse)responseEntity.getBody()).getMessage(),"No Parser found.");
    }
}
