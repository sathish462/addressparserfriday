package com.friday.addressParser.Util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.friday.addressParser.dto.AddressDto;
import com.friday.addressParser.testData.ESampleTestData;
import java.io.IOException;

public class UtilTest {

    public static <T> T jsonToObject(String json, Class<T> clazz) throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        return mapper.readValue(json, clazz);
    }

    public static <T> String objectToJson(T object) throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return mapper.writeValueAsString(object);
    }

    public static AddressDto getTestAddressDTO()
    {
        return ESampleTestData.SIMPLE_ADDRESS_1.getKey();
    }

    public static AddressDto getInvalidAddressDTO()
    {
        return new AddressDto("");
    }

}
