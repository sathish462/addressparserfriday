package com.friday.addressParser.service;

import com.friday.addressParser.dto.AddressDto;
import com.friday.addressParser.exception.EmptyAddressException;
import com.friday.addressParser.exception.ParserNotFoundException;
import com.friday.addressParser.testData.ESampleTestData;
import com.friday.addressParser.dto.ResponseAddressDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class ParseAddressServiceTest {

    private ParseAddressService parseAddressService ;
    public static final String NO_PARSER_ADDRESS = "JunkAddress@Junk";

    @BeforeEach
    public void setUp()
    {
        parseAddressService = new ParseAddressServiceImpl( new AddressProcessorServiceImpl());
    }

    /**
     * Test Simple address formats.
     */
    @Test
    public void simpleAddressTestCases()
    {
        ResponseAddressDto resultDto = parseAddressService.getParsedAddress(ESampleTestData.SIMPLE_ADDRESS_1.getKey());
        assertEquals(ESampleTestData.SIMPLE_ADDRESS_1.getValue(),resultDto);

        resultDto = parseAddressService.getParsedAddress(ESampleTestData.SIMPLE_ADDRESS_2.getKey());
        assertEquals(ESampleTestData.SIMPLE_ADDRESS_2.getValue(),resultDto);

        resultDto = parseAddressService.getParsedAddress(ESampleTestData.SIMPLE_ADDRESS_3.getKey());
        assertEquals(ESampleTestData.SIMPLE_ADDRESS_3.getValue(),resultDto);

    }

    /**
     * Test complex address formats.
     */
    @Test
    public void complicatedAddressTestCases()
    {
        ResponseAddressDto resultDto = parseAddressService.getParsedAddress(ESampleTestData.COMPLICATED_ADDRESS_1.getKey());
        assertEquals(ESampleTestData.COMPLICATED_ADDRESS_1.getValue(),resultDto);

        resultDto = parseAddressService.getParsedAddress(ESampleTestData.COMPLICATED_ADDRESS_2.getKey());
        assertEquals(ESampleTestData.COMPLICATED_ADDRESS_2.getValue(),resultDto);
    }

    /**
     *  Test International address formats.
     */
    @Test
    public void complexInternationalAddressTestCases()
    {
        ResponseAddressDto resultDto = parseAddressService.getParsedAddress(ESampleTestData.INTERNATIONAL_ADDRESS_1.getKey());
        assertEquals(ESampleTestData.INTERNATIONAL_ADDRESS_1.getValue(),resultDto);

        resultDto = parseAddressService.getParsedAddress(ESampleTestData.INTERNATIONAL_ADDRESS_2.getKey());
        assertEquals(ESampleTestData.INTERNATIONAL_ADDRESS_2.getValue(),resultDto);

        resultDto = parseAddressService.getParsedAddress(ESampleTestData.INTERNATIONAL_ADDRESS_3.getKey());
        assertEquals(ESampleTestData.INTERNATIONAL_ADDRESS_3.getValue(),resultDto);

        resultDto = parseAddressService.getParsedAddress(ESampleTestData.INTERNATIONAL_ADDRESS_4.getKey());
        assertEquals(ESampleTestData.INTERNATIONAL_ADDRESS_4.getValue(),resultDto);
    }

    /**
     * Test Empty address - must throw Custom exception - EmptyAddressException
     */
    @Test
    public void givenAddressEMPTYTest()
    {
        assertThrows(EmptyAddressException.class, () -> parseAddressService.getParsedAddress(new AddressDto()));
    }

    /**
     * Test NULL address - must throw Custom exception - EmptyAddressException
     */
    @Test
    public void givenAddressNULLTest()
    {
        assertThrows(EmptyAddressException.class, () -> parseAddressService.getParsedAddress(new AddressDto(null)));
    }

    /**
     * Test Invalid address - must throw Custom exception - ParserNotFoundException
     */
    @Test
    public void invalidInputForParserTest()
    {
        assertThrows(ParserNotFoundException.class, () -> parseAddressService.getParsedAddress(new AddressDto(NO_PARSER_ADDRESS)));
    }
    

}
