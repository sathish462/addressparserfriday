package com.friday.addressParser.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

/**
 * POJO to get input request.
 * @author :Sathish Kumar
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressDto {

    @JsonProperty(required = true)
    @NotEmpty
    @NotBlank
    private String address;

    public AddressDto() {
    }

    public AddressDto(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
