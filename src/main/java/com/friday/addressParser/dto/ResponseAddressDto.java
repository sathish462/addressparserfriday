package com.friday.addressParser.dto;

import java.util.Objects;
/**
 * POJO for HTTPResponse as JSON.
 * @author :Sathish Kumar
 */
public class ResponseAddressDto {

    private String street;

    private String houseNumber;

    public ResponseAddressDto(String street, String houseNumber) {
        this.street = street;
        this.houseNumber = houseNumber;
    }

    public ResponseAddressDto() {
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ResponseAddressDto)) return false;
        ResponseAddressDto that = (ResponseAddressDto) o;
        return getStreet().equals(that.getStreet()) && getHouseNumber().equals(that.getHouseNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStreet(), getHouseNumber());
    }
}
