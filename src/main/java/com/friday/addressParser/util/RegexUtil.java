package com.friday.addressParser.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtil {

    /**
     *  Regex to identify the pattern of Deutschland address.
     *  1st group ^([^0-9]+) - matches any character that are not in the set.
     *  2nd group ([a-zA-Z_0-9]+\t\n\x0b\r\f?[a-zA-Z]?) - matches all characters appended with space/new line.
     *         ?- Matches to the previous characters followed by alphabets.
     */
    public static final String DEFAULT_ADDRESS = "^([^0-9]+) ([a-zA-Z_0-9]+\\s?[a-zA-Z]?)";

    /**
     * Regex to identify the pattern of International Address.
     * 1st group ^([a-zA-Z_0-9]+) - matches all characters.
     * 2nd group ([^0-9]+) matches any character that are not in the set.
     */
    public static final String COMPLICATED_ADDRESS = "^([a-zA-Z_0-9]+) ([^0-9]+)";

    /**
     * Regex to identify Exact string "No" or "Number"
     * 1st group is before the string.
     * 2nd group is after the string.
     */
    public static final String INTERNATIONAL_ADDRESS = "(?= (?i)(No|Number)\\s\\d+)";

    public static String removeSpecialCharacters( String address )
    {
        return address.replaceAll(","," ");
    }

    public static Matcher getMatcher( String regex, String address ) {
        Pattern pattern = Pattern.compile( regex );
        return pattern.matcher( address );
    }
}
