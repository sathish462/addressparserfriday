package com.friday.addressParser.service;

import com.friday.addressParser.dto.ResponseAddressDto;
import com.friday.addressParser.util.RegexUtil;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.regex.Matcher;

@Service
public class AddressProcessorServiceImpl implements AddressProcessorService{

    @Override
    public ResponseAddressDto parseDefaultAddress(Matcher matcher)
    {
        String[] parsedAddress = new String[2];
        for (int i = 1; i <= matcher.groupCount(); i++) {
            parsedAddress[i - 1] = matcher.group(i).trim();
        }
        return new ResponseAddressDto(parsedAddress[0], parsedAddress[1]);
    }

    @Override
    public ResponseAddressDto parseComplicatedAddress(Matcher matcher)
    {
        String[] parsedAddress = new String[2];
        for ( int i = 1; i <= matcher.groupCount(); i++ ) {
            final String data = matcher.group(i).trim();
            if ( NumberUtils.isCreatable(data) ) {
                parsedAddress[1] = data;
            }
            parsedAddress[0] = data;
        }
        return new ResponseAddressDto(parsedAddress[0], parsedAddress[1]);
    }

    @Override
    public ResponseAddressDto parseInternationalAddress(String address, Matcher matcher)
    {
        String[] parsedAddress = Arrays.stream(address.split(RegexUtil.INTERNATIONAL_ADDRESS)).map(String::trim).toArray(String[]::new);
        return new ResponseAddressDto(parsedAddress[0], parsedAddress[1]);
    }
}
