package com.friday.addressParser.service;

import com.friday.addressParser.dto.ResponseAddressDto;

import java.util.regex.Matcher;

public interface AddressProcessorService {

    public ResponseAddressDto parseDefaultAddress(Matcher matcher);

    public ResponseAddressDto parseComplicatedAddress(Matcher matcher);

    public ResponseAddressDto parseInternationalAddress(String address, Matcher matcher);

}
