package com.friday.addressParser.service;

import com.friday.addressParser.dto.AddressDto;
import com.friday.addressParser.dto.ResponseAddressDto;
import com.friday.addressParser.exception.EmptyAddressException;

import java.util.regex.Matcher;

public interface ParseAddressService {

    public ResponseAddressDto getParsedAddress(AddressDto addressDto) throws EmptyAddressException;

    public Matcher getDefaultAddressMatcher(String address);

    public Matcher getComplicatedAddressMatcher(String address);

    public Matcher getInternationalAddressMatcher(String address);

}
