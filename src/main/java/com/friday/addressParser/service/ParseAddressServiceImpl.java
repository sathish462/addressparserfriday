package com.friday.addressParser.service;

import com.friday.addressParser.dto.AddressDto;
import com.friday.addressParser.dto.ResponseAddressDto;
import com.friday.addressParser.exception.EmptyAddressException;
import com.friday.addressParser.exception.ParserNotFoundException;
import com.friday.addressParser.util.RegexUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;

@Service
public class ParseAddressServiceImpl implements ParseAddressService {

    private static final String EMPTY_ADDRESS_MESSAGE = "Address is Empty or Null and cannot be parsed";

    @Autowired
    private AddressProcessorService addressProcessorService;

    @Autowired
    public ParseAddressServiceImpl( AddressProcessorService addressProcessorService ) {
        this.addressProcessorService = addressProcessorService;
    }

    @Override
    public ResponseAddressDto getParsedAddress(AddressDto addressDto) throws EmptyAddressException
    {
        String address = addressDto.getAddress();

        if( address == null || address.isEmpty() )
            throw new EmptyAddressException(EMPTY_ADDRESS_MESSAGE);

        address = RegexUtil.removeSpecialCharacters(address);

        Matcher matcher = this.getInternationalAddressMatcher(address);
        if( matcher.find() )
            return addressProcessorService.parseInternationalAddress( address, matcher);

        matcher = this.getDefaultAddressMatcher(address);
        if( matcher.find() )
            return addressProcessorService.parseDefaultAddress(matcher);

        matcher = this.getComplicatedAddressMatcher(address);
        if( matcher.find() )
            return addressProcessorService.parseComplicatedAddress(matcher);

        throw new ParserNotFoundException(address);
    }

    @Override
    public Matcher getDefaultAddressMatcher(String address) {
        return RegexUtil.getMatcher(RegexUtil.DEFAULT_ADDRESS, address);
    }

    @Override
    public Matcher getComplicatedAddressMatcher(String address) {
        return RegexUtil.getMatcher( RegexUtil.COMPLICATED_ADDRESS, address );
    }

    @Override
    public Matcher getInternationalAddressMatcher(String address) {
        return RegexUtil.getMatcher(RegexUtil.INTERNATIONAL_ADDRESS, address);
    }
}
