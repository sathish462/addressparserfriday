package com.friday.addressParser.controller;

import com.friday.addressParser.dto.AddressDto;
import com.friday.addressParser.dto.ResponseAddressDto;
import com.friday.addressParser.exception.EmptyAddressException;
import com.friday.addressParser.service.ParseAddressService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

/**
 * Controller for address parser API
 * two possible responses 200 and 400. Mapped to "/v1/addressParser".
 *
 * @author :Sathish Kumar
 */
@RestController
@RequestMapping("v1/addressParser")
@ApiOperation(value = "Parse concatenated address to a normalized address")
@ApiResponses({
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 400, message = "BAD REQUEST")
})
public class AddressParserController {

    @Autowired
    private ParseAddressService parseService;

    @PostMapping
    public ResponseAddressDto parseAddress(@Valid @RequestBody AddressDto addressDTO ) throws EmptyAddressException
    {
        return parseService.getParsedAddress(addressDTO);
    }
}
