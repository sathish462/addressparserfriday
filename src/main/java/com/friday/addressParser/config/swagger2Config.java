package com.friday.addressParser.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * swagger configuration class to configure and test the running API Address Parser
 *
 * @author :Sathish Kumar
 */
@Configuration
@EnableSwagger2
public class swagger2Config {

    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("com.friday.addressParser.controller"))
                .paths(PathSelectors.regex("/.*")).build().apiInfo(apiEndPointInfo());
    }

    private ApiInfo apiEndPointInfo()
    {
        return new ApiInfoBuilder().title("Address Parser API").build();
    }
}
