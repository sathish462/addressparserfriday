package com.friday.addressParser.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Empty address cannot be parsed.")
public class EmptyAddressException extends RuntimeException{

    public final long serialVersionUID = 1L;

    public EmptyAddressException(String message) {
        super(message);
    }
}
