package com.friday.addressParser.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "No Parser found" )
public class ParserNotFoundException extends RuntimeException{

    public final long serialVersionUID = 1L;

    public ParserNotFoundException(String address) {
        super( String.format( "Parser not found for given address :  %s", address ) );
    }
}
